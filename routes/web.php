<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  ini_set('xdebug.var_display_max_depth', '10');
  ini_set('xdebug.var_display_max_children', '256');
  ini_set('xdebug.var_display_max_data', '1024');

  //TODO move the various request / response with kie server into
  //it's own package
  
  $baseHost = "http://192.168.122.11:8080/kie-server/services/rest/";
  $containerToExec = "my-awesome-container-new2";
  $processToExec = "project.buttons";
  $auth = [
    'jboss', 'bpms'
  ];

  $options = [
    'auth' => $auth,
    'headers'  => ['content-type' => 'application/json', 'Accept' => 'application/json'],
  ];

  /**
   * Set up requests for getting server details
   */
  $requestsToExec = [
    [
      'index' => 'server',
      'name' => "Server Details",
      'path' => $baseHost . "server",
      'verb' => "GET",
      'options' => $options
    ],
    [
      'index' => 'kie-containers',
      "name" => "Container Details",
      'path' => $baseHost . 'server/containers',
      'verb' => "GET",
      'options' => $options
    ]
  ];
  $requestAndResponses = batchExec($requestsToExec);

  /**
   * Get container details based on previous responses
   */
  // get details for each container...
  // first unwrap the response a little bit
  $kieContainersRaw = json_decode($requestAndResponses['kie-containers']['response']->getBody(), true);
  $kieContainers = $kieContainersRaw['result']['kie-containers']['kie-container']; 
  
  $requestsToExec = [];
  foreach ($kieContainers as $kieContainer) {
    $requestsToExec[] = [
      'index' => 'container-details-' . $kieContainer['container-id'],
      'name' => "Container Details for " . $kieContainer['container-id'],
      'path' => $baseHost . 'server/containers/'  . $kieContainer['container-id'],
      'verb' => "GET",
      "options" => $options
    ];
    $requestsToExec[] = [
      'index' => 'container-process-' . $kieContainer['container-id'],
      'name' => "Process Details for " . $kieContainer['container-id'],
      'path' => $baseHost . 'server/queries/containers/'  . $kieContainer['container-id'] . '/processes/definitions',
      'verb' => "GET",
      "options" => $options
    ];
    //
  }
  $requestAndResponses = array_merge($requestAndResponses, batchExec($requestsToExec));

  /**
   * Get process definition's variables
   */
  $requestToExec = [
    'index' => "container-" . $containerToExec . "-process-" . $processToExec . '-variables',
    'name' => "Process Variables for " . $processToExec . " deployed via " . $containerToExec,
    'verb' => "GET",
    'path' => $baseHost . 'server/containers/' . $containerToExec . "/processes/definitions/" . $processToExec . '/variables',
    'options' => $options
  ];
  $requestAndResponses = array_merge($requestAndResponses, batchExec([$requestToExec]));


  /**
   * Start a process
   */
  $requestToExec = [
    'index' => "exec-container-" . $containerToExec . "-process-" . $processToExec,
    'name' => "Starting " . $processToExec . " deployed via " . $containerToExec,
    'verb' => "POST",
    'path' => $baseHost . 'server/containers/' . $containerToExec . "/processes/" . $processToExec . '/instances',
    'options' => $options
  ];

  $requestAndResponses = array_merge($requestAndResponses, batchExec([$requestToExec]));

  $processInstanceId = json_decode($requestAndResponses[$requestToExec['index']]['response']->getBody());

  /**
   * Get process instance variables
   */
  //server/containers/{id}/processes/instances/{pInstanceId}/variables - GET
  $requestToExec = [
    'index' => "container-" . $containerToExec . "-process-" . $processToExec . '-instance-id-' . $processInstanceId . '-variables',
    'name' => "Process Instance ".$processInstanceId." Variables for " . $processToExec . " deployed via " . $containerToExec,
    'verb' => "GET",
    'path' => $baseHost . 'server/containers/' . $containerToExec . "/processes/instances/" . $processInstanceId . '/variables',
    'options' => $options
  ];
  $requestAndResponses = array_merge($requestAndResponses, batchExec([$requestToExec]));

  /**
   * Get process details for each container based on previous responses
   */
  // server/queries/definitions => returns all process definitions
  // server/queries/containers/processes/definition

  //

  //get the task id...
  // GET: server/queries/tasks/instances/process/{pInstanceId}
  $requestToExec = [
    "index" => "process-instance-tasks-" . $processInstanceId,
    "name" => "Tasks for process instance " . $processInstanceId,
    "verb" => "GET",
    "path" => $baseHost . "server/queries/tasks/instances/process/" . $processInstanceId,
    "options" => $options,
  ];
  $requestAndResponses = array_merge($requestAndResponses, batchExec([$requestToExec]));

  $taskSummary = json_decode($requestAndResponses[$requestToExec['index']]['response']->getBody(), true);
  
  $taskId = $taskSummary['task-summary'][0]['task-id'];

  // need to claim it first?
  $requestToExec = [ 
    'index' => 'task-claim-'.$taskId,
    'name' => 'Claim the task ' . $taskId,
    'verb' => 'PUT',
    'path' => $baseHost . 'server/containers/' . $containerToExec . '/tasks/' . $taskId . '/states/claimed',
    "options" => $options,
  ];
  $requestAndResponses = array_merge($requestAndResponses, batchExec([$requestToExec]));

  // print out status after claim
  $requestToExec = [
    "index" => "process-instance-tasks-" . $processInstanceId . '-after-claim',
    "name" => "Tasks for process instance " . $processInstanceId . " (after claim)",
    "verb" => "GET",
    "path" => $baseHost . "server/queries/tasks/instances/process/" . $processInstanceId,
    "options" => $options,
  ];
  $requestAndResponses = array_merge($requestAndResponses, batchExec([$requestToExec]));

  //PUT: server/containers/{id}/tasks/{tInstanceId}/contents/output 
  // Saves content on task with given id that belongs to given container

  $requestToExec = [ 
    'index' => 'task-set-action-'.$taskId,
    'name' => 'Set action on task ' . $taskId,
    'verb' => 'PUT',
    'path' => $baseHost . 'server/containers/' . $containerToExec . '/tasks/' . $taskId . '/contents/output',
    "options" => array_merge($options, ['json' => ['action' => 'approve']]),
  ];
  $requestAndResponses = array_merge($requestAndResponses, batchExec([$requestToExec]));

  $requestToExec = [
    'index' => "container-" . $containerToExec . "-process-" . $processToExec . '-instance-id-' . $processInstanceId . '-variables-after-set',
    'name' => "Process Instance ".$processInstanceId." Variables for " . $processToExec . " deployed via " . $containerToExec,
    'verb' => "GET",
    'path' => $baseHost . 'server/containers/' . $containerToExec . "/processes/instances/" . $processInstanceId . '/variables',
    'options' => $options
  ];
  $requestAndResponses = array_merge($requestAndResponses, batchExec([$requestToExec]));

  // $requestToExec = [ 
  //   'index' => 'task-complete-'.$taskId,
  //   'name' => 'Complete the task ' . $taskId,
  //   'verb' => 'PUT',
  //   'path' => $baseHost . 'server/containers/' . $containerToExec . '/tasks/' . $taskId . '/states/completed',
  //   "options" => $options,
  // ];
  // $requestAndResponses = array_merge($requestAndResponses, batchExec([$requestToExec]));

  $viewData = [
    'requestAndResponses' => $requestAndResponses
  ];

  return view('kie-api-overview', $viewData);
});


function batchExec($requestsToExec) {
  $client = new \GuzzleHttp\Client();
  $requestAndResponses = [];
  foreach ($requestsToExec as $requestToExec) {
    $request = new GuzzleHttp\Psr7\Request($requestToExec['verb'], $requestToExec['path']);
    $response = $client->send($request, $requestToExec['options']);
    $index = $requestToExec['index'];
    // TODO add logic for case of index not defined
    $requestAndResponses[$index] = [
      'name' => $requestToExec['name'],
      'request' => $request,
      'response' => $response
    ];
  }
  return $requestAndResponses;
}


