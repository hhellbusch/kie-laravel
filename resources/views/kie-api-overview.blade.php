<html>
<body>

<?php foreach($requestAndResponses as $requestResponse): ?>
  <h1><?php echo $requestResponse['name']; ?></h1>
  <!-- <h3>Request: </h3> -->
  
  <strong><?php echo $requestResponse['request']->getMethod(); ?></strong>: <?php echo $requestResponse['request']->getRequestTarget(); ?>

  <h3>Response: </h3>
  <pre> <?php 
    $json = json_decode($requestResponse['response']->getBody());
    echo json_encode($json, JSON_PRETTY_PRINT);
    ?> </pre>
  <hr />
<?php endforeach; ?>
  
</body>
</html>
